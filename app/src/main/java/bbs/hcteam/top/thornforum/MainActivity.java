package bbs.hcteam.top.thornforum;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;

public class MainActivity extends AppCompatActivity {

    private FrameLayout mainContent;
    private FragmentTransaction fragment;
    private FragmentManager fragmentManager;
    private Fragment homeFragment,plateFragment,personFragment;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            fragment = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment.replace(R.id.content,homeFragment);
                    fragment.commit();
                    return true;
                case R.id.navigation_dashboard:
                    fragment.replace(R.id.content,plateFragment);
                    fragment.commit();
                    return true;
                case R.id.navigation_notifications:
                    BmobUser mBmobUser = BmobUser.getCurrentUser();
                    if (mBmobUser==null){
                        Intent intent = new Intent(MainActivity.this,LoginActivity2.class);
                        startActivity(intent);
                        return false;
                    }

                    fragment.replace(R.id.content,personFragment);
                    fragment.commit();
                    return true;
            }
            return false;
        }

    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainContent = (FrameLayout) findViewById(R.id.content);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        fragmentManager = getSupportFragmentManager();
        homeFragment=new HomeFragmentActivity();
        plateFragment=new PlateFragmentActivity();
        personFragment=new PersonFragmentActivity();

        Bmob.initialize(this,"1583eaf7f0c9de18386a4cece1c5c890","Bmob");

    }

}
